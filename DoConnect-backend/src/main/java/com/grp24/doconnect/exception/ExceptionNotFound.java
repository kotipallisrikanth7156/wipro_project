package com.grp24.doconnect.exception;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public class ExceptionNotFound extends RuntimeException {
	
	public ExceptionNotFound(String errorMsg) {
		super();
		this.errorMsg = errorMsg;
	}
	private static final long serialVersionUID = 1L;
	private String errorMsg;

}
